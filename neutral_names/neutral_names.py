#!/usr/bin/env python
# /home/atollye/current/programming_exercises/neutral_names/neutral_names.py

"""
    Эта программа выводит гендерно-нейтральные имена (т.е. подходящие и
    для мальчиков, и для девочек), которые использовались в Америке за
    прошедшие сто лет, и является решением к упражнению № 155 из книги
    Stephenson, "The Python workbook"
"""

import sys
import os


def main_loop():

    """main_loop() обеспечивает взимодействие с пользователем и
    обработку пользовательского инпута """

    print("\n \nЭта программа выводит имена, которые использовались и для" +
          " мальчиков, \nи для девочек по годам. \n ")

    while True:
        inpt = input("Введите, пожалуйста, год   ")
        if inpt.isdigit() and len(inpt) == 4:
            year_str = inpt
            year_num = int(year_str)
            if year_num in list(range(1900, 2013)):
                year_num = str(year_num)
                neutralNames = find_gender_neutral(year_num)
                if neutralNames:
                    names_str = ','.join(neutralNames)
                    print("\nВ этом году и девочек, и мальчиков" +
                          " называли {}".format(names_str))
                else:
                    print("\nВ этом году одинаковых имен" +
                          " у девочек и мальчиков не было")
            else:
                print("\nПо этому году не данных")
        else:
            print("\nВведен неверный год")
        wanna_continue()


def find_gender_neutral(year_num):

    """ find_gender_neutral(year_num) ищет в файле из папки data,
    соответствующем выбранному году, гендерно-нейтральные имена и
    возвращает их функции main_loop() """

    dataDirectory = os.path.join(os.path.dirname(
                                  os.path.abspath(__file__)), 'data')

    boys_filename = year_num + "_BoysNames.txt"
    boys_fullpath = os.path.join(dataDirectory, boys_filename)
    file_boys = open(boys_fullpath, "r")
    boys_names = set()
    for line in file_boys:
        ln = file_boys.readline()
        name, quantity = ln.split()
        boys_names.add(name)

    girls_filename = str(year_num) + "_GirlsNames.txt"
    girls_fullpath = os.path.join(dataDirectory, girls_filename)
    file_girls = open(girls_fullpath, "r")
    girls_names = set()
    for line in file_girls:
        ln = file_girls.readline()
        name, quantity = ln.split()
        girls_names.add(name)
    neutralNames = girls_names & boys_names

    neutralNames = list(neutralNames)
    return neutralNames


def wanna_continue():

    """wanna_continue() обеспечивает выход из программы при желании
    пользователя """

    ans = input("\nХотите выйти из программы? Yes\\No \n \n")
    ans = ans.lower()
    if ans == "yes":
        sys.exit()


if __name__ == '__main__':
    main_loop()
