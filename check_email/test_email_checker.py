#!/usr/bin/env python3
# /home/atollye/current/programming_exercises/check_email/test_email_checker.py

import random
import string

import unittest

from email_checker import check_email

class TestEmailChecker(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass

# 1 e-mail состоит из имени и доменной части, эти части разделяются
# символом "@"; 
    def test_1(self): 
        text = 'alyonka@petrova.ru' 
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_2(self):  
        text = 'alyonkapetrova.ru'
        result = check_email(text)
        self.assertEqual(result, [0,0,0,0,0,0,0])


# 2 a) доменная часть не короче 3 символов
    def test_3(self):
        text = 'alyonkapetro@v.a'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_4(self):
        text = 'alyonkapetrova@.r'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])


#2 б) доменная часть не длиннее 256 символов
    def test_5(self): #255 знаков в домене
        #252 символа + ".ru" = 255:
        domain = "".join("ro" for x in range(126))+".ru" 
        text = "alyonka@" + domain
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_6(self):  #256 знаков
        #252 символа + ".com" = 256
        domain = "".join("ro" for x in range(126))+".com" 
        text = "alyonka@" + domain
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_7(self): #257 знаков
        #252 символа + ".info" = 257
        domain = "".join("ro" for x in range(126))+".info" 
        text = "alyonka@" + domain
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])


#2 в)  доменная часть является набором непустых строк,
    def test_8(self):
        text = 'alyonka@666.78'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_9(self):
        text = 'alyonkapetrova@ru.'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])

    def test_10(self):
        text = 'alyonkapetrova@.ru'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])

    def test_11(self):
        text = 'alyonka@ya ndex.ru'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])



# 2 г)  ...состоящих из символов a-z 0-9_- и разделенных точкой;
    def test_12(self):
        text = 'alyonka@79pe-36-tro__va.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_13(self):
        text = "alyonka@pet'rova.ru"
        result = check_email(text)
        self.assertEqual(result,  [1,0,0,0,0,0,0])

    def test_14(self):
        text = "alyonka@pe@trova.ru"
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])

    def test_15(self):
        text = "alyonka@peTrova.ru"
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])

    def test_16(self):
        text = 'alyonka@petrovaru'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])

    def test_17(self):
        text = 'alyonka@pe.tro.va'
        result = check_email(text)
        self.assertEqual(result, [1,0,0,0,0,0,0])


#3 Каждый компонент доменной части не может начинаться или заканчиваться
# символом "-"; 
    def test_18(self):
        text = '-alyonka-@pet-ro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_19(self):
        text = 'alyonka@-petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,0,0,0,0,0])

    def test_20(self):
        text = 'alyonka@petro.-va'
        result = check_email(text)
        self.assertEqual(result, [1,1,0,0,0,0,0])

    def test_21(self):
        text = ' alyonka@petro-.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,0,0,0,0,0])

    def test_22(self): #сбой
        text = 'alyonka@petro.va-'
        result = check_email(text)
        self.assertEqual(result, [1,1,0,0,0,0,0])




# 4 а) имя (до @) не длиннее 128 символов,
    def test_23(self): #127 символов в имени
        username = "".join(random.choice(string.ascii_lowercase) for x in range(127))
        text = username+"@petrova.ru"
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_24(self): # 128 символов в имени
        username = "".join(random.choice(string.ascii_lowercase) for x in range(128))
        text = username+"@petrova.ru"
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_25(self): #129 символов в имени
        username = "".join(random.choice(string.ascii_lowercase) for x in range(129))
        text = username+"@petrova.ru"
        result = check_email(text)
        self.assertEqual(result, [1,1,1,0,0,0,0])

# 4 б) имя состоит из символов a-z0-9"._-; (и символов !,: -- баг задания)
    def test_26(self):
        text = 'al777yonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_27(self):
        text = 'al":yonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,0,0])

    def test_28(self):
        text = 'alyonk*a@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,0,0,0,0])

    def test_29(self):
        text = 'alyoNka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,0,0,0,0])


# 5. В имени не допускаются две точки подряд; 
    def test_30(self):
        text = 'a.l.y.o.n.k.a@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])
    def test_31(self):
        text = 'al..yonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,0,0,0])


# 6. Если в имени есть двойные кавычки ", то они должны быть парными; 
    def test_32(self):
        text = '''alyonka""@petro.va'''
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])
    def test_33(self):
        text = '''al""yon""ka@petrova.ru'''
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])
    def test_34(self):
        text = '''alyonka"@petro.va'''
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,0,0])
    def test_35(self):
        text = '''al"!"yo"nka@petrova.ru'''
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,0,0])


# 7. В имени могут встречаться символы "!,:", но только между парными
# двойными кавычками.
    def test_36(self):
        text = 'al"!"yonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_37(self):
        text = 'alyonka":"@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_38(self):
        text = '","alyonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])


    def test_39(self):
        text = 'al"!"yo","nka@petrova.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1]) 

    def test_40(self):
        text = 'al"!"yo","nka@petrova.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_41(self):
        text = 'aly",!:"onka@yandex.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

    def test_42(self):
        text = 'al"!!"y"!:"onka@yandex.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,1])

al"!!"y"!;"onka@yandex.ru

    def test_43(self):
        text = 'al!yonka@petro.va'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,0])

    def test_44(self):
        text = 'alyo""!nka@petrova.ru'
        result = check_email(text)
        self.assertEqual(result, [1,1,1,1,1,1,0])



if __name__ == '__main__':
    unittest.main()








