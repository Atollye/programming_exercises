#!/usr/bin/env python3
# /home/atollye/current/programming_exercises/check_email/email_checker.py

r"""
Функция email_checker проверяет введенный пользователем e-mail на соответствие
следующим правилам:

1. e-mail состоит из имени и доменной части, эти части разделяются
 символом "@";
2. доменная часть не короче 3 символов и не длиннее 256, является
 набором непустых строк, состоящих из символов a-z 0-9_- и разделенных
 точкой;
3. каждый компонент доменной части не может начинаться или заканчиваться
символом "-";
4. имя (до @) не длиннее 128 символов, состоит из символов a-z0-9"._-;
5. в имени не допускаются две точки подряд;
6. если в имени есть двойные кавычки ", то они должны быть парными;
7. в имени могут встречаться символы "!,:", но только между парными
двойными кавычками.


Функция работает как самостоятельный модуль или может быть импортирована
"""

import re


def check_email(email):
    requirements = [0, 0, 0, 0, 0, 0, 0]

    # пункт 1
    re_1 = re.compile(r'(.)+@(.)+')
    if re.fullmatch(re_1, email):
        requirements[0] = 1
        username, domain = email.split(sep='@', maxsplit=1)

        # пункт 2
        if len(domain) >= 3 and len(domain) <= 256:
            re_2 = re.compile(r'([a-z0-9_-])+\.([a-z0-9_-])+')
            if re.fullmatch(re_2, domain):
                requirements[1] = 1

                # пункт 3
                domain_name, extension = domain.split(sep='.',
                                                      maxsplit=1)
                if not (domain_name.startswith('-') or
                        domain_name.endswith('-') or
                        extension.startswith('-') or
                        extension.endswith('-')
                        ):
                    requirements[2] = 1

                    # пункт 4
                    re_4 = re.compile(r'([a-z0-9".!,:_-]){1,128}')
                    if re.fullmatch(re_4, username):
                        requirements[3] = 1

                        # пункт 5
                        re_5 = re.compile(r'(.)*\.\.(.)*')
                        if not re.fullmatch(re_5, username):
                            requirements[4] = 1

                            # пункт 6
                            quantity = username.count('''"''')
                            if quantity % 2 == 0:
                                requirements[5] = 1

                                # пункт 7
                                if(('!' in username) or (',' in username) or
                                   (':' in username)):
                                    re_7 = re.compile(r'"(.?)*([!:,])+(.?)*"')
                                    username_without = re.sub(re_7, '',
                                                              username)

                                    if not (('!' in username_without) or
                                            (',' in username_without) or
                                            (':' in username_without)):
                                            requirements[6] = 1
                                else:
                                    requirements[6] = 1

    return requirements


if __name__ == '__main__':
    email = input("Enter an email to check     ")
    requirements = check_email(email)
    if all(requirements) == 1:
        result = 'Email is correct'
    else:
        result = 'Email is incorrect'
    print(result)
